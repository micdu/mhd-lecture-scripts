import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.widgets import Slider, Button

twopi = 2 * np.pi
npoints = 95                                                           
aminor = 0.5
aspect = 5
rmaj = aminor * aspect  
perturb = 0.05
elong = 1.5

theta = np.repeat(np.arange(npoints) / (npoints - 1.) * twopi,npoints).reshape(npoints,npoints)
phi = np.repeat(np.arange(npoints) / (npoints - 1.) * twopi,npoints).reshape(npoints,npoints).transpose()

amp = perturb
m0 = 0
n0 = 0
r = np.zeros((npoints,npoints)) + aminor
rcyl = r * np.cos(theta) + rmaj
zcyl = r *  np.sin(theta) * elong
delta = r * 0.

fig = plt.figure()
ax1 = fig.add_subplot(121)
ax1.set_aspect('equal')
ax2 = fig.add_subplot(122,projection='3d')
#ax2.set_aspect('equal')
plt.subplots_adjust(left = 0.25,bottom = 0.25)
l, = ax1.plot(rcyl[:,0],zcyl[:,0])

axm = plt.axes([0.25,0.1,0.65,0.03])
axn = plt.axes([0.25,0.15,0.65,0.03])
#axref = plt.axes([0.25,0.05,0.65,0.03])

nval = Slider(axm, 'n',0,10,valinit=m0)
mval = Slider(axn, 'm',0,40,valinit=n0)
#refresh = Button(axref,'Refresh 3D plot')

def update(val):
    m = round(mval.val)
    n = round(nval.val)
    r = np.zeros((npoints,npoints)) + aminor
    temp = amp * np.sin(theta * m + phi * n)
    delta = temp / np.max(temp)
    r = r + temp
    rcyl = r * np.cos(theta) + rmaj
    zcyl = r *  np.sin(theta) * elong
    l.set_xdata(rcyl[:,0])
    l.set_ydata(zcyl[:,0])
    #plt.plot(rcyl[:,0],zcyl[:,0])
    #plt.show()
    #fig.canvas.draw_idle()

#def plot3d(event):
    #print('Refreshing')
    x = rcyl * np.cos(phi)
    y = rcyl * np.sin(phi)
    z = zcyl
    ax2.clear()
    norm = matplotlib.colors.Normalize(-1,1)
    m = plt.cm.ScalarMappable(norm=norm,cmap='jet')
    m.set_array([])
    fcolors = m.to_rgba(delta)

    surf=ax2.plot_surface(x,y,z,facecolors=fcolors,rstride=1,cstride=1,vmin=-1,vmax=1,shade=False)
    max_range = np.array([x.max()-x.min(), y.max()-y.min(), z.max()-z.min()]).max()
    Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(x.max()+x.min())
    Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(y.max()+y.min())
    Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(z.max()+z.min())
    # Comment or uncomment following both lines to test the fake bounding box:
    for xb, yb, zb in zip(Xb, Yb, Zb):
        ax2.plot([xb], [yb], [zb], 'w')
    fig.canvas.draw_idle()

mval.on_changed(update)
nval.on_changed(update)
#refresh.on_clicked(plot3d)

plt.show()
