import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

ves = np.loadtxt('diaggeom.vessel')

a1 = 1. / 10.
a2 = 0.8
a3 = -0.5

npts = 128

r,z = np.meshgrid(np.linspace(0.,2.5,npts),np.linspace(-1.5,1.5,npts),indexing='ij')

fig,ax = plt.subplots()
plt.subplots_adjust(right=0.7)
ax.set_aspect('equal')
ax.set_xlim((0,2.5))

psi_pla = a1 * r**4 + a2 * z**2
psi_vac = a3 * r**2
psi = psi_pla + psi_vac

max = np.amax(psi)
min = np.amin(psi)
levels = np.linspace(min,max,101)

ax.plot(ves[:,0],ves[:,1])
ax.contour(r,z,psi,levels=levels)
#contour_axis = plt.gca()

axa1 = plt.axes([0.75,0.15,0.2,0.03])
axa2 = plt.axes([0.75,0.10,0.2,0.03])
axa3 = plt.axes([0.75,0.05,0.2,0.03])
axc1 = plt.axes([0.75,0.20,0.2,0.03])
axc2 = plt.axes([0.75,0.25,0.2,0.03])
axc3 = plt.axes([0.75,0.30,0.2,0.03])

a1val = Slider(axa1,'a1',0,1,valinit=a1)
a2val = Slider(axa2,'a2',0,1,valinit=a2)
a3val = Slider(axa3,'a3',-1,0,valinit=a3)
c1val = Slider(axc1,'c1',-1,1,valinit=0.)
c2val = Slider(axc2,'c2',-1,1,valinit=0.)
c3val = Slider(axc3,'c3',-1.2,1.2,valinit=0.)

r0 = [1.4,1.4,3.5,3.5]
z0 = [1.4,-1.4,0.7,-0.7]
def update(val):
    a1 = a1val.val
    a2 = a2val.val
    a3 = a3val.val
    
    c1 = c1val.val
    c2 = c2val.val
    c3 = c3val.val
    c = [c1,c2,c3,c3]
    
    psi_pla = a1 * r**4 + a2 * z**2
    psi_vac = a3 * r**2
    psi_ext = psi_pla * 0.
    for i,dum in enumerate(r0):
        psi_ext = psi_ext + c[i] * np.log((r - r0[i])**2 + (z-z0[i])**2)
    
    
    psi = psi_pla + psi_vac + psi_ext
    max = np.amax(psi)
    min = np.amin(psi)
    levels = np.linspace(min,max,101)
    ax.clear()
    ax.set_aspect('equal')
    ax.set_xlim((0,2.5))
    ax.plot(ves[:,0],ves[:,1])
    ax.contour(r,z,psi,levels=levels)
    fig.canvas.draw_idle()
   #contour_axis.clear()
   #contour_axis.contour(r,z,psi,levels=levels)
   #plt.draw()

a1val.on_changed(update)
a2val.on_changed(update)
a3val.on_changed(update)
c1val.on_changed(update)
c2val.on_changed(update)
c3val.on_changed(update)

plt.show()





